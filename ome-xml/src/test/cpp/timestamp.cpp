/*
 * #%L
 * OME-XML C++ library for working with OME-XML metadata structures.
 * %%
 * Copyright © 2006 - 2016 Open Microscopy Environment:
 *   - Massachusetts Institute of Technology
 *   - National Institutes of Health
 *   - University of Dundee
 *   - Board of Regents of the University of Wisconsin-Madison
 *   - Glencoe Software, Inc.
 * Copyright © 2018 Quantitative Imaging Systems, LLC
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of any organization.
 * #L%
 */

#include <ome/xml/config-internal.h>

#include <ome/xml/model/primitives/Timestamp.h>

#include <ome/model-test/test.h>

#include <sstream>
#include <stdexcept>
#include <iostream>

using time_point = std::chrono::system_clock::time_point;

using ome::xml::model::primitives::Timestamp;

enum result
  {
    PASS, // Always passes
    FAIL, // Always fails
    PLAT  // Platform-dependent
  };

class TimestampTestParameters
{
public:
  std::string input;
  std::string output;
  time_t      seconds;
  uint32_t    nanoseconds;
  result      parse_result;
  bool        chrono_throws;
  bool        chrono_same;
  time_point  timepoint;

  TimestampTestParameters(const std::string& input,
                          const std::string& output,
                          time_t             seconds,
                          uint32_t           nanoseconds,
                          result             parse_result,
                          bool               chrono_throws):
    input(input),
    output(output),
    seconds(seconds),
    nanoseconds(nanoseconds),
    parse_result(parse_result),
    chrono_throws(chrono_throws),
    chrono_same(true),
    timepoint()
  {
    timepoint = std::chrono::system_clock::from_time_t(seconds);
    std::chrono::nanoseconds nsec_duration(nanoseconds);
    timepoint += std::chrono::duration_cast<std::chrono::system_clock::duration>(nsec_duration);

    time_t check = std::chrono::system_clock::to_time_t(timepoint);

    chrono_same = (seconds == check);
  }
};

template<class charT, class traits>
inline std::basic_ostream<charT,traits>&
operator<< (std::basic_ostream<charT,traits>& os,
            const TimestampTestParameters& params)
{
  return os << params.input;
}

class TimestampTest : public ::testing::TestWithParam<TimestampTestParameters>
{
};

TEST_P(TimestampTest, ConstructString)
{
  const TimestampTestParameters& params = GetParam();

  if (params.parse_result == FAIL)
    {
      ASSERT_THROW(Timestamp(params.input), std::runtime_error);
    }
  else
    {
      try
        {
          Timestamp t{params.input};
          ASSERT_EQ(params.seconds, t.get_time());
          ASSERT_EQ(params.nanoseconds, t.get_nsecs());
          ASSERT_EQ(params.seconds, static_cast<time_t>(t));
          if (!params.chrono_throws)
            {
              ASSERT_EQ(params.timepoint, static_cast<time_point>(t));
            }
        }
      catch (const std::runtime_error&)
        {
          if (params.parse_result == PLAT)
            return;
          throw;
        }
    }
}

TEST_P(TimestampTest, ConstructBrokenDown)
{
  const TimestampTestParameters& params = GetParam();

  tm *btime;
#ifdef OME_XML_HAVE_GMTIME_R
  struct tm t;
  btime = gmtime_r(&params.seconds, &t);
#else
  btime = gmtime(&params.seconds);
#endif

  if (params.parse_result != PASS)
    {
      // Skip bad test data.
      return;
    }
  else
    {
      ASSERT_TRUE(btime != nullptr);
      Timestamp t{*btime, uint32_t(params.nanoseconds)};
      ASSERT_EQ(params.seconds, t.get_time());
      ASSERT_EQ(params.nanoseconds, t.get_nsecs());
      ASSERT_EQ(params.seconds, static_cast<time_t>(t));
      if (!params.chrono_throws)
        {
          ASSERT_EQ(params.timepoint, static_cast<time_point>(t));
        }
    }
}

TEST_P(TimestampTest, ConstructTimeT)
{
  const TimestampTestParameters& params = GetParam();

  Timestamp t{time_t(params.seconds), uint32_t(params.nanoseconds)};
  ASSERT_EQ(params.seconds, t.get_time());
  ASSERT_EQ(params.nanoseconds, t.get_nsecs());
  ASSERT_EQ(params.seconds, static_cast<time_t>(t));
  if (params.parse_result == PASS && !params.chrono_throws)
    {
      ASSERT_EQ(params.timepoint, static_cast<time_point>(t));
    }
}

TEST_P(TimestampTest, ConstructTimePoint)
{
  const TimestampTestParameters& params = GetParam();

  if (params.parse_result != PASS)
    {
      // Skip bad test data.
      return;
    }

  Timestamp t{params.timepoint};
  if (!params.chrono_same)
    {
      ASSERT_NE(params.seconds, t.get_time());
      ASSERT_NE(params.seconds, static_cast<time_t>(t));
      // Don't compare timepoint; it won't necessarily round-trip if
      // the second fraction is less than the earliest representable
      // time.
    }
  else
    {
      ASSERT_EQ(params.seconds, t.get_time());
      std::chrono::nanoseconds nsec_duration(params.nanoseconds);
      ASSERT_EQ(std::chrono::duration_cast<std::chrono::system_clock::duration>(nsec_duration),
                std::chrono::duration_cast<std::chrono::system_clock::duration>(std::chrono::nanoseconds(t.get_nsecs())));
      ASSERT_EQ(params.seconds, static_cast<time_t>(t));
      ASSERT_EQ(params.timepoint, static_cast<time_point>(t));
    }
}

TEST_P(TimestampTest, CopyConstruct)
{
  const TimestampTestParameters& params = GetParam();

  if (params.parse_result == PASS)
    {
      Timestamp orig{params.input};
      Timestamp t(orig);
      ASSERT_EQ(params.seconds, t.get_time());
      std::chrono::nanoseconds nsec_duration(params.nanoseconds);
      ASSERT_EQ(std::chrono::duration_cast<std::chrono::system_clock::duration>(nsec_duration),
                std::chrono::duration_cast<std::chrono::system_clock::duration>(std::chrono::nanoseconds(t.get_nsecs())));
      ASSERT_EQ(params.seconds, static_cast<time_t>(t));
      if (!params.chrono_throws)
        {
          ASSERT_EQ(params.timepoint, static_cast<time_point>(t));
        }
    }
}

TEST_P(TimestampTest, AssignString)
{
  const TimestampTestParameters& params = GetParam();

  Timestamp t;
  if (params.parse_result == FAIL)
    {
      ASSERT_THROW(t = params.input, std::runtime_error);
    }
  else
    {
      try
        {
          t = params.input;
          ASSERT_EQ(params.seconds, t.get_time());
          std::chrono::nanoseconds nsec_duration(params.nanoseconds);
          ASSERT_EQ(std::chrono::duration_cast<std::chrono::system_clock::duration>(nsec_duration),
                    std::chrono::duration_cast<std::chrono::system_clock::duration>(std::chrono::nanoseconds(t.get_nsecs())));
          ASSERT_EQ(params.seconds, static_cast<time_t>(t));
          if (!params.chrono_throws)
            {
              ASSERT_EQ(params.timepoint, static_cast<time_point>(t));
            }
        }
      catch (const std::runtime_error&)
        {
          if (params.parse_result == PLAT)
            return;
          throw;
        }
    }
}

TEST_P(TimestampTest, AssignBrokenDown)
{
  const TimestampTestParameters& params = GetParam();

  tm *btime;
#ifdef OME_XML_HAVE_GMTIME_R
  struct tm t;
  btime = gmtime_r(&params.seconds, &t);
#else
  btime = gmtime(&params.seconds);
#endif

  if (params.parse_result != PASS)
    {
      // Skip bad test data.
      return;
    }
  else
    {
      ASSERT_TRUE(btime != nullptr);
      Timestamp t;
      t = *btime;
      ASSERT_EQ(params.seconds, t.get_time());
      ASSERT_EQ(params.seconds, static_cast<time_t>(t));
    }
}

TEST_P(TimestampTest, AssignTimeT)
{
  const TimestampTestParameters& params = GetParam();

  if (params.parse_result != PASS)
    {
      // Skip bad test data.
      return;
    }

  Timestamp t;
  t = time_t(params.seconds);
  ASSERT_EQ(params.seconds, t.get_time());
  ASSERT_EQ(0U, t.get_nsecs());
  ASSERT_EQ(params.seconds, static_cast<time_t>(t));
}

TEST_P(TimestampTest, AssignTimePoint)
{
  const TimestampTestParameters& params = GetParam();

  if (params.parse_result != PASS)
    {
      // Skip bad test data.
      return;
    }

  Timestamp t;
  if (!params.chrono_same)
    {
      t = params.timepoint;
      ASSERT_NE(params.seconds, t.get_time());
      ASSERT_NE(params.seconds, static_cast<time_t>(t));
      // Don't compare timepoint; it won't necessarily round-trip if
      // the second fraction is less than the earliest representable
      // time.
    }
  else
    {
      t = params.timepoint;
      ASSERT_EQ(params.seconds, t.get_time());
      std::chrono::nanoseconds nsec_duration(params.nanoseconds);
      ASSERT_EQ(std::chrono::duration_cast<std::chrono::system_clock::duration>(nsec_duration),
                std::chrono::duration_cast<std::chrono::system_clock::duration>(std::chrono::nanoseconds(t.get_nsecs())));
      ASSERT_EQ(params.seconds, static_cast<time_t>(t));
      ASSERT_EQ(params.timepoint, static_cast<time_point>(t));
    }
}

TEST_P(TimestampTest, CopyAssign)
{
  const TimestampTestParameters& params = GetParam();

  if (params.parse_result == PASS)
    {
      Timestamp orig{params.input};
      Timestamp t = orig;
      ASSERT_EQ(params.seconds, t.get_time());
      std::chrono::nanoseconds nsec_duration(params.nanoseconds);
      ASSERT_EQ(std::chrono::duration_cast<std::chrono::system_clock::duration>(nsec_duration),
                std::chrono::duration_cast<std::chrono::system_clock::duration>(std::chrono::nanoseconds(t.get_nsecs())));
      ASSERT_EQ(params.seconds, static_cast<time_t>(t));
      if (!params.chrono_throws)
        {
          ASSERT_EQ(params.timepoint, static_cast<time_point>(t));
        }
    }
}

TEST_P(TimestampTest, StreamOutput)
{
  const TimestampTestParameters& params = GetParam();

    if (params.parse_result == PASS)
    {
      Timestamp t(params.input);
      std::ostringstream os;
      os << t;
      ASSERT_EQ(os.str(), params.output);
    }
}

TEST_P(TimestampTest, StreamInput)
{
  const TimestampTestParameters& params = GetParam();

  Timestamp t;
  std::istringstream is(params.input);
  if (params.parse_result == FAIL)
    {
      is >> t;
      ASSERT_TRUE(!is);
    }
  else
    {
      is >> t;
      if (params.parse_result == PASS)
        {
          ASSERT_TRUE(!!is);
          std::ostringstream os;
          os << t;
          ASSERT_EQ(os.str(), params.output);
        }
    }
}

TEST_P(TimestampTest, TimeFromEpoch)
{
  const TimestampTestParameters& params = GetParam();

  if (params.parse_result == PASS && !params.chrono_throws)
    {
      auto epoch = std::chrono::system_clock::from_time_t(0);
      Timestamp ts(params.input);

      auto duration = static_cast<Timestamp::time_point_type>(ts) - epoch;

      auto seconds = std::chrono::duration_cast<std::chrono::seconds>(duration);
      auto systicks = std::chrono::duration_cast<std::chrono::system_clock::duration>(duration);
      auto rounded_systicks = std::chrono::duration_cast<std::chrono::system_clock::duration>(seconds);

      auto relative_systicks = systicks - rounded_systicks;
      auto csysticks = std::chrono::duration_cast<std::chrono::system_clock::duration>(relative_systicks);

      std::chrono::nanoseconds nsec_duration(params.nanoseconds);
      ASSERT_EQ(std::chrono::seconds(params.seconds), seconds);
      ASSERT_EQ(std::chrono::duration_cast<std::chrono::system_clock::duration>(nsec_duration),
                csysticks);
    }
}

std::vector<TimestampTestParameters> params =
  {
    //                                                                                                    chrono
    // input                               output                            seconds     nanos      parse throws
    { "2003-08-26T19:46:38",               "2003-08-26T19:46:38Z",           1061927198,         0, PASS, false},
    { "2003-08-26T19:46:38.762",           "2003-08-26T19:46:38.762Z",       1061927198, 762000000, PASS, false},
    { "2003-08-26T19:46:38.762Z",          "2003-08-26T19:46:38.762Z",       1061927198, 762000000, PASS, false},
    { "2003-08-26T19:46:38.762822-0300",   "2003-08-26T22:46:38.762822Z",    1061937998, 762822000, PASS, false},
    { "2003-08-26T19:46:38.76284729+0400", "2003-08-26T15:46:38.762847290Z", 1061912798, 762847290, PASS, false},
    { "2003-08-26T19:46:38.762-1130",      "2003-08-27T07:16:38.762Z",       1061968598, 762000000, PASS, false},
    { "2011-10-20T15:07:14",               "2011-10-20T15:07:14Z",           1319123234,         0, PASS, false},
    { "2011-10-20T15:07:14.312",           "2011-10-20T15:07:14.312Z",       1319123234, 312000000, PASS, false},
    { "2011-10-20T15:07:14Z",              "2011-10-20T15:07:14Z",           1319123234,         0, PASS, false},
    { "2011-10-20T15:07:14.632Z",          "2011-10-20T15:07:14.632Z",       1319123234, 632000000, PASS, false},
    { "1971-05-06T16:34:59Z",              "1971-05-06T16:34:59Z",             42395699,         0, PASS, false},
    // May be too far in past for system to handle.
    { "1200-12-04T13:12:12",               "1200-12-04T13:12:12Z" ,        -24269626068,         0, PLAT,  true},
    { "invalid",                           "" /* Invalid */,                          0,         0, FAIL,  false},
    { "20111020T093010.654",               "" /* Invalid */,                          0,         0, FAIL,  false},
    { "2003-08-26T19:46:38.762+",          "" /* Time difference is invalid */,       0,         0, FAIL,  false},
    { "2003-08-26T19:46:38.762-",          "" /* Time difference is invalid */,       0,         0, FAIL,  false},
    { "2011-10-20T09:30:10-05:00",         "" /* Time difference is invalid */,       0,         0, FAIL,  false}
  };

// Disable missing-prototypes warning for INSTANTIATE_TEST_CASE_P;
// this is solely to work around a missing prototype in gtest.
#ifdef __GNUC__
#  if defined __clang__ || defined __APPLE__
#    pragma GCC diagnostic ignored "-Wmissing-prototypes"
#  endif
#  pragma GCC diagnostic ignored "-Wmissing-declarations"
#endif

INSTANTIATE_TEST_CASE_P(TimestampVariants, TimestampTest, ::testing::ValuesIn(params));
