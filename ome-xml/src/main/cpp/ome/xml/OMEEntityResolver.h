/*
 * #%L
 * OME-XML C++ library for working with OME-XML metadata structures.
 * %%
 * Copyright © 2015 - 2016 Open Microscopy Environment:
 *   - Massachusetts Institute of Technology
 *   - National Institutes of Health
 *   - University of Dundee
 *   - Board of Regents of the University of Wisconsin-Madison
 *   - Glencoe Software, Inc.
 * Copyright © 2019 Codelibre Consulting Limited
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of any organization.
 * #L%
 */

#ifndef OME_XML_MODEL_OMEENTITYRESOLVER_H
#define OME_XML_MODEL_OMEENTITYRESOLVER_H

#include <ome/compat/filesystem.h>

#include <ome/xml/types.h>
#include <ome/xml/version.h>

#ifdef OME_HAVE_XERCES_DOM
#include <ome/xerces-util/EntityResolver.h>
#elif OME_HAVE_QT5_DOM
#include <QtXml/QXmlEntityResolver>
#ifdef OME_HAVE_QT5_XSLT
#include <QtXmlPatterns/QAbstractUriResolver>
#endif
#endif

namespace ome
{
  namespace xml
  {

#ifdef OME_HAVE_XERCES_DOM
    using BaseEntityResolver = ome::common::xml::EntityResolver;
#elif OME_HAVE_QT5_DOM
    using BaseEntityResolver = QXmlEntityResolver;
#endif

    /**
     * Entity resolver for the OME schemas.
     *
     * This resolver will resolve local copies of all the OME schemas
     * distributed as part of the OME Data Model (ome-model).
     */
    class OMEEntityResolver : public BaseEntityResolver
#if OME_HAVE_QT5_XSLT
                            , public QAbstractUriResolver
#endif
    {
    public:
      /// Constructor.
      OMEEntityResolver();

      /// Destructor.
      ~OMEEntityResolver();

#ifdef OME_HAVE_XERCES_DOM
      /**
       * Resolve an entity.
       *
       * @param resource the resource to resolve.
       * @returns an input source containing the cached content, or
       * null if the resource was not cached.
       */
      xercesc::InputSource *
      resolveEntity(xercesc::XMLResourceIdentifier* resource);
#elif OME_HAVE_QT5_DOM
      /**
       * Resolve an entity.
       *
       * @param publicId the public identifier of the resource to resolve.
       * @param systemId the system identifier of the resource to resolve.
       * @param ret an input source containing the cached content, or
       * null if the resource was not found and must be resolved by
       * the reader.
       * @returns @c true if successful; @c false if an error
       * occurred.
       */
      bool
      resolveEntity(const QString& publicId,
                    const QString& systemId,
                    QXmlInputSource *& ret) override;

      QString
      errorString() const override;

#if OME_HAVE_QT5_XSLT
      /**
       * Resolve a URI.
       *
       * @param relative URI relative to baseURI
       * @param baseURI the base URI.
       * @returns the resolved URI.
       */
      QUrl
      resolve(const QUrl &relative,
              const QUrl &baseURI) const override;
#endif
#endif

      /**
       * Register a file with the entity resolver.
       *
       * @param id the XML system ID of the entity.
       * @param file the filename of the entity.
       */
      void
      registerEntity(const std::string&                   id,
                     const ome::compat::filesystem::path& file);

      /**
       * Register a catalog with the entity resolver.
       *
       * @param file the filename of the catalog.
       */
      void
      registerCatalog(const ome::compat::filesystem::path& file);

    private:
      /// Mapping from system ID to filesystem path.
      typedef std::map<std::string, ome::compat::filesystem::path> entity_path_map_type;
      /// Mapping from system ID to XML data.
      typedef std::map<std::string, std::string> entity_data_map_type;

      /**
       * Get input source from file.
       *
       * Open and read the contents of the file, then return this as
       * an InputSource.  Use cached content if possible.
       *
       * @param resource the resource to resolve.
       * @returns the input source for the file, or null on failure.
       */
      XMLInputSource *
      getSource(const std::string& resource);

    private:
      /// Map of registered system IDs to filesystem paths.
      entity_path_map_type entity_path_map;
      /// Map of system IDs to cached XML data.
      entity_data_map_type entity_data_map;
    };

  }
}

#endif // OME_XML_MODEL_OMEENTITYRESOLVER_H

/*
 * Local Variables:
 * mode:C++
 * End:
 */
