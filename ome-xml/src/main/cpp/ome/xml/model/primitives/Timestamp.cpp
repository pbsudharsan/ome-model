/*
 * #%L
 * OME-XML C++ library for working with OME-XML metadata structures.
 * %%
 * Copyright © 2006 - 2016 Open Microscopy Environment:
 *   - Massachusetts Institute of Technology
 *   - National Institutes of Health
 *   - University of Dundee
 *   - Board of Regents of the University of Wisconsin-Madison
 *   - Glencoe Software, Inc.
 * Copyright © 2018 Quantitative Imaging Systems, LLC
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of any organization.
 * #L%
 */

#include <sstream>

#include <ome/xml/config-internal.h>

#include <ome/xml/model/primitives/Timestamp.h>

namespace ome
{
  namespace xml
  {
    namespace model
    {
      namespace primitives
      {

        Timestamp::Timestamp():
          Timestamp(std::chrono::system_clock::now())
        {
        }

        Timestamp::Timestamp(const std::string& timestamp):
          time()
        {
          std::istringstream is(timestamp);
          is.imbue(std::locale::classic());

          // Parse common fields.
          tm t;
          uint32_t nanosecs = 0;

          char sep1, sep2, sepT, sep3, sep4;

          is >> t.tm_year >> sep1
             >> t.tm_mon >> sep2
             >> t.tm_mday >> sepT
             >> t.tm_hour >> sep3
             >> t.tm_min >> sep4
             >> t.tm_sec;
          t.tm_year -= 1900;
          t.tm_mon -= 1;

          if (sep1 != '-' || sep2 != '-' ||
              sepT != 'T' ||
              sep3 != ':' || sep4 != ':')
            {
              throw std::runtime_error("Failed to parse timestamp: Incorrect format");
            }

          t.tm_wday = 0;
          t.tm_yday = 0;
          t.tm_isdst = 0;

#if defined(OME_XML_HAVE_TIMEGM)
          time_t parsed_time = timegm(&t);
#elif defined(OME_XML_HAVE__MKGMTIME)
          time_t parsed_time = _mkgmtime(&t);
#else
#error Timestamp cannot be converted to UTC time_t
          //time_t parsed_time = mktime(&t);
#endif
          if (parsed_time == time_t(-1))
            {
              throw std::runtime_error("Failed to parse timestamp: Invalid date or time");
            }
          // Parse fraction of second, if present, as nanoseconds.
          std::char_traits<char>::int_type sep5 = is.peek();
          if (sep5 == '.')
            {
              is.ignore();

              // Count leading digits
              for (int pos = 0; pos < 9; ++pos)
                {
                  std::char_traits<char>::int_type digit = is.peek();
                  nanosecs *= 10;
                  if (digit >= '0' && digit <= '9')
                    {
                      is.ignore();
                      nanosecs += (digit - '0');
                    }
                }
            }

          if (is)
            {
              // Check for zone offset
              std::char_traits<char>::int_type tztype = is.peek();
              if(tztype != std::char_traits<char>::eof())
                {
                  if (tztype == 'Z')
                    {
                      is.ignore(); // Drop above from istream
                      // If Z, we're already using UTC, so don't apply numeric offsets
                    }
                  else if (tztype == '-' || tztype == '+')
                    {
                      is.ignore(); // Drop above from istream
                      // Check that the next 4 characters are only numeric
                      int32_t offset = 0;
                      for (int i = 0; i < 4; ++i)
                        {
                          std::char_traits<char>::int_type digit = is.peek();
                          offset *= 10;
                          if (digit >= '0' && digit <= '9')
                            {
                              is.ignore();
                              offset += (digit - '0');
                            }
                          else
                            {
                              throw std::runtime_error("Failed to parse timestamp: Invalid zone offset");
                            }
                        }
                      if (is)
                        {
                          const int32_t hours = offset / 100U;
                          const int32_t minutes = offset % 100U;
                          int32_t soffset = (hours * 60U * 60U) + (minutes * 60U);
                          // Apply offset
                          if (tztype == '+')
                            {
                              parsed_time -= soffset;
                            }
                          else
                            {
                              parsed_time += soffset;
                            }
                        }
                    }
                }
            }

          if (is.rdbuf()->in_avail() > 0)
            throw std::runtime_error("Failed to parse timestamp: incomplete parsing of string");

          *this = Timestamp(parsed_time, nanosecs);
        }

        Timestamp::Timestamp(const tm& time,
                             uint32_t  nsecs):
          time(),
          nsecs(nsecs)
        {
          // mktime is modifying, so copy
          tm tc(time);
#if defined(OME_XML_HAVE_TIMEGM)
          this->time = timegm(&tc);
#elif defined(OME_XML_HAVE__MKGMTIME)
          this->time = _mkgmtime(&tc);
#else
#error Timestamp cannot be converted to UTC time_t
          //this->time = mktime(&tc);
#endif
          if (this->time == time_t(-1))
            {
              throw std::runtime_error("Failed to parse timestamp: Invalid date or time");
            }
        }

        Timestamp::Timestamp(time_point_type time_point):
          Timestamp(std::chrono::system_clock::to_time_t(time_point),
                    std::chrono::duration_cast<std::chrono::nanoseconds>(time_point.time_since_epoch() % std::chrono::seconds(1)).count())
        {
        }

        Timestamp::operator tm () const
        {
          tm *btime;
#ifdef OME_XML_HAVE_GMTIME_R
          struct tm t;
          btime = gmtime_r(&time, &t);
#else
          btime = gmtime(&time);
#endif
          if (btime)
            {
              return *btime;
            }
          else
            {
              throw std::runtime_error("Timestamp not representable as broken-down time");
            }
        }

      }
    }
  }
}
