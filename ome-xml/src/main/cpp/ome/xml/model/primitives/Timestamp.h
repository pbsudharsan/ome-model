/*
 * #%L
 * OME-XML C++ library for working with OME-XML metadata structures.
 * %%
 * Copyright © 2006 - 2016 Open Microscopy Environment:
 *   - Massachusetts Institute of Technology
 *   - National Institutes of Health
 *   - University of Dundee
 *   - Board of Regents of the University of Wisconsin-Madison
 *   - Glencoe Software, Inc.
 * Copyright © 2018 Quantitative Imaging Systems, LLC
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of any organization.
 * #L%
 */

#ifndef OME_XML_MODEL_PRIMITIVES_TIMESTAMP_H
#define OME_XML_MODEL_PRIMITIVES_TIMESTAMP_H

#include <chrono>
#include <cstdint>
#include <ctime>
#include <iomanip>
#include <iosfwd>
#include <string>
#include <stdexcept>

namespace ome
{
  namespace xml
  {
    namespace model
    {
      namespace primitives
      {

        /**
         * An ISO-8601 timestamp.
         *
         * The timestamp will have at least microsecond precision and
         * on most systems will have nanosecond precision.
         *
         * The timestamp is stored as a date and time with second
         * precision, plus an additional nanosecond value to store
         * precise sub-second timing.
         */
        class Timestamp {
        public:
          /// Underlying system time representation.
          using time_point_type = std::chrono::system_clock::time_point;

          /**
           * Construct a Timestamp (defaults to current UTC time).
           */
          Timestamp();

          /**
           * Copy a Timestamp.
           */
          Timestamp(const Timestamp& copy) = default;

          /**
           * Construct a Timestamp from an ISO-8601 date string.
           *
           * Will parse up to nanosecond precision; additional digits
           * will be discarded.
           *
           * @param timestamp an ISO-8601-formatted string.
           * @throws std::runtime_error if invalid or outside the
           * supported range of time_t.
           */
          explicit
          Timestamp(const std::string& timestamp);

          /**
           * Construct a Timestamp from broken-down time.
           *
           * The time elapsed since the epoch (seconds and
           * nanoseconds).
           *
           * @param time the system time (seconds).
           * @param nsecs the  (nanoseconds).
           */
          explicit
          Timestamp(const tm& time,
                    uint32_t  nsecs = 0U);

          /**
           * Construct a Timestamp from system time.
           *
           * The time elapsed since the epoch (seconds and
           * nanoseconds).
           *
           * @param time the system time (seconds).
           * @param nsecs the  (nanoseconds).
           */
          explicit
          Timestamp(time_t   time,
                    uint32_t nsecs = 0U) noexcept:
            time(time),
            nsecs(nsecs)
          {
          }

          /**
           * Construct a Timestamp from system time point.
           *
           * Will have at least microsecond precision on most systems,
           * and nanosecond precision on some systems.
           *
           * @note If the time point may be rounded or truncated if
           * outside the range supported by @c time_t .
           *
           * @param time_point the system time point.
           */
          explicit
          Timestamp(time_point_type time_point);

          /**
           * Assign timestamp from timestamp.
           *
           * @param rhs the timestamp to assign.
           * @returns the new timestamp value.
           */
          Timestamp&
          operator= (const Timestamp& rhs) = default;

          /**
           * Assign timestamp from ISO-8601 date string.
           *
           * @param rhs the date to assign.
           * @returns the new timestamp value.
           * @throws std::runtime_error if invalid or outside the
           * supported range of time_t.
           */
          Timestamp&
          operator= (const std::string& rhs)
          {
            *this = Timestamp(rhs);
            return *this;
          }

          /**
           * Assign timestamp from broken-down time.
           *
           * @param rhs the broken-down time to assign.
           * @returns the new timestamp value.
           * @throws std::runtime_error if invalid or outside the
           * supported range of time_t.
           */
          Timestamp&
          operator= (const tm& rhs)
          {
            *this = Timestamp(rhs);
            return *this;
          }

          /**
           * Assign timestamp from system time.
           *
           * @param rhs the system time to assign.
           * @returns the new timestamp value.
           */
          Timestamp&
          operator= (const time_t& rhs)
          {
            *this = Timestamp(rhs);
            return *this;
          }

          /**
           * Assign timestamp from system time point.
           *
           * @note If the time point may be rounded or truncated if
           * outside the range supported by @c time_t .
           *
           * @param rhs the system time point to assign.
           * @returns the new timestamp value.
           */
          Timestamp&
          operator= (const time_point_type& rhs)
          {
            *this = Timestamp(rhs);
            return *this;
          }

          const time_t&
          get_time() const noexcept
          {
            return time;
          }

          uint32_t
          get_nsecs() const noexcept
          {
            return nsecs;
          }

          /**
           * Get the wrapped system time value as broken time.
           *
           * Returns the system time.
           * @throws std::runtime_error if conversion fails.
           */
          operator tm () const;

          /**
           * Get the wrapped system time value as system time point.
           *
           * Returns the system time.
           */
          inline
          operator time_t () const noexcept
          {
            return time;
          }

          /**
           * Get the wrapped system time value.
           *
           * Returns the system time.
           * @throws std::runtime_error if conversion fails.
           */
          inline
          operator time_point_type () const
          {
            time_t tt = static_cast<time_t>(*this);
            std::chrono::system_clock::time_point tp = std::chrono::system_clock::from_time_t(tt);
            time_t tt_check = std::chrono::system_clock::to_time_t(tp);
            if (tt != tt_check)
              {
                throw std::logic_error("Timestamp not representable as std::chrono::system_time::timepoint");
              }
            std::chrono::nanoseconds nsec_duration(nsecs);
            tp += std::chrono::duration_cast<std::chrono::system_clock::duration>(nsec_duration);
            return tp;
          }

        private:
          /// The system time (second precision).
          time_t time;
          /// The nanosecond fraction
          uint32_t nsecs;
        };

        /**
         * Output Timestamp to output stream.
         *
         * @param os the output stream.
         * @param timestamp the timestamp to output.
         * @returns the output stream.
         */
        template<class charT, class traits>
        inline std::basic_ostream<charT,traits>&
        operator<< (std::basic_ostream<charT,traits>& os,
                    const Timestamp& timestamp)
        {
          const tm t = static_cast<tm>(timestamp);
          const uint32_t nsecs = timestamp.get_nsecs();

          auto millis = nsecs / 1000000U;
          auto micros = (nsecs / 1000U) % 1000U;
          auto nanos = nsecs % 1000U;

          os << std::setw(4) << std::setfill('0') << (t.tm_year + 1900)
             << '-'
             << std::setw(2) << std::setfill('0') << (t.tm_mon + 1)
             << '-'
             << std::setw(2) << std::setfill('0') << t.tm_mday
             << 'T'
             << std::setw(2) << std::setfill('0') << t.tm_hour
             << ':'
             << std::setw(2) << std::setfill('0') << t.tm_min
             << ':'
             << std::setw(2) << std::setfill('0') << t.tm_sec;

          if(millis || micros || nanos)
            {
              os << '.'
                 << std::setw(3) << std::setfill('0')
                 << millis;
            }
          if(micros || nanos)
            {
              os << std::setw(3) << std::setfill('0')
                 << micros;
            }
          if(nanos)
            {
              os << std::setw(3) << std::setfill('0')
                 << nanos;
            }
          os << 'Z';

          return os;
        }

        /**
         * Set Timestamp from input stream.
         *
         * @param is the input stream.
         * @param timestamp the Timestamp to set.
         * @returns the input stream.
         */
        template<class charT, class traits>
        inline std::basic_istream<charT,traits>&
        operator>> (std::basic_istream<charT,traits>& is,
                    Timestamp& timestamp)
        {
          std::istream::sentry s(is);
          if (s)
            {
              std::string stamp;
              is >> stamp;
              try
                {
                  timestamp = Timestamp(stamp);
                }
              catch (const std::exception& e)
                {
                  is.setstate(std::ios::failbit);
                }
            }
          return is;
        }

      }
    }
  }
}

#endif // OME_XML_MODEL_PRIMITIVES_TIMESTAMP_H

/*
 * Local Variables:
 * mode:C++
 * End:
 */
