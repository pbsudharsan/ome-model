/*
 * #%L
 * OME-XML C++ library for working with OME-XML metadata structures.
 * %%
 * Copyright © 2006 - 2016 Open Microscopy Environment:
 *   - Massachusetts Institute of Technology
 *   - National Institutes of Health
 *   - University of Dundee
 *   - Board of Regents of the University of Wisconsin-Madison
 *   - Glencoe Software, Inc.
 * Copyright © 2019 Codelibre Consulting Limited
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of any organization.
 * #L%
 */

#include <ome/common/log.h>

#include <ome/xml/model/Reference.h>
#include <ome/xml/model/detail/OMEModelObject.h>
#include <ome/xml/types.h>

#ifdef OME_HAVE_XERCES_DOM
#include <ome/xerces-util/String.h>
#endif

using ome::common::LOG_WARN;

namespace ome
{
  namespace xml
  {
    namespace model
    {
      namespace detail
      {

        OMEModelObject::OMEModelObject ():
          ::ome::xml::model::OMEModelObject()
        {
        }

        OMEModelObject::~OMEModelObject ()
        {
        }

        OMEModelObject::OMEModelObject (const OMEModelObject& /* copy */):
          ::ome::xml::model::OMEModelObject()
        {
          // Nothing to copy.
        }

        bool
        OMEModelObject::validElementName(const std::string& /* name */) const
        {
          return false;
        }

        DOMElement
        OMEModelObject::asXMLElement (DOMDocument& document) const
        {
          DOMElement element;
          if(elementName() != "OME")
            {
#ifdef OME_HAVE_XERCES_DOM
              element = document.createElementNS(getXMLNamespace(), "${klass.name}");
#elif OME_HAVE_QT5_DOM
              element = document.createElementNS(QString::fromUtf8(getXMLNamespace().c_str()),
                                                 QString::fromUtf8("${klass.name}"));
#endif
            }
          else
            {
#ifdef OME_HAVE_XERCES_DOM
              element = document.getDocumentElement();
#elif OME_HAVE_QT5_DOM
              element = document.documentElement();
#endif
            }
          asXMLElementInternal(document, element);
          return element;
        }

        void
        OMEModelObject::asXMLElement (DOMDocument& document,
                                      DOMElement&  element) const
        {
          asXMLElementInternal(document, element);
        }

        void
        OMEModelObject::asXMLElementInternal (DOMDocument& /* document */,
                                              DOMElement&  /* element */) const
        {
        }

        void
        OMEModelObject::update (const DOMElement&            /* element */,
                                ::ome::xml::model::OMEModel& /* model */)
        {
        }

        bool
        OMEModelObject::link (std::shared_ptr<Reference>&                         reference,
                              std::shared_ptr<::ome::xml::model::OMEModelObject>& object)
        {
          LOG_WARN("{} unable to handle reference of type {} for {}",
                   elementName(), reference->elementName(), object->elementName());
          return false;
        }

        std::vector<DOMElement>
        OMEModelObject::getChildrenByTagName (const DOMElement&  parent,
                                              const std::string& name)
        {
          return getChildrenByTagNameNS(parent, name, std::string());
        }

        std::vector<DOMElement>
        OMEModelObject::getChildrenByTagNameNS (const DOMElement&  parent,
                                                const std::string& name,
                                                const std::string& ns)
        {
          std::vector<DOMElement> ret;

#ifdef OME_HAVE_XERCES_DOM
          common::xml::String xmlname(name);
          common::xml::String xmlns(ns);
          for (xercesc::DOMNode *pos = parent.get()->getFirstChild();
               pos != 0;
               pos = pos->getNextSibling())
            {
              try
                {
                  // This pointer check is unnecessary--the Element
                  // class would throw; but this avoids the need to
                  // throw and catch many std::logic_error exceptions
                  // during document processing.
                  if (dynamic_cast<const xercesc::DOMElement *>(pos))
                    {
                      DOMElement child(pos, false);
                      if (child)
                        {
                          if (xmlname != common::xml::String(child->getLocalName()))
                            {
                              continue;
                            }
                          if (!ns.empty() && xmlns != common::xml::String(child->getNamespaceURI()))
                            {
                              continue;
                            }
                          ret.push_back(child);
                        }
                    }
                }
              catch (std::logic_error&)
                {
                  // Not an Element.
                }
            }
#elif OME_HAVE_QT5_DOM
          QString xmlname = QString::fromUtf8(name.c_str());
          QString xmlns = QString::fromUtf8(ns.c_str());
          for (DOMNode pos = parent.firstChild();
               !pos.isNull();
               pos = pos.nextSibling())
          {
            DOMElement child = pos.toElement();
            if (!child.isNull())
              {
                if (xmlname != child.localName())
                  {
                    continue;
                  }
                if (!ns.empty() && xmlns != child.namespaceURI())
                  {
                    continue;
                  }
                ret.push_back(child);
            }
          }
#endif
          return ret;
        }

        std::string
        OMEModelObject::stripNamespacePrefix (const std::string& value) {
          std::string ret;
          std::string::size_type i = value.find_last_of(':');
          if (i != std::string::npos && ++i < value.size())
            ret = value.substr(i);
          else
            ret = value;
          return ret;
        }

      }
    }
  }
}
