/*
 * #%L
 * OME-XML C++ library for working with OME-XML metadata structures.
 * %%
 * Copyright © 2015 - 2016 Open Microscopy Environment:
 *   - Massachusetts Institute of Technology
 *   - National Institutes of Health
 *   - University of Dundee
 *   - Board of Regents of the University of Wisconsin-Madison
 *   - Glencoe Software, Inc.
 * Copyright © 2018 Quantitative Imaging Systems, LLC
 * Copyright © 2019 Codelibre Consulting Limited
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of any organization.
 * #L%
 */

#include <ome/xml/Document.h>
#include <ome/xml/OMEEntityResolver.h>

#include <istream>

#include <fmt/format.h>

namespace
{

#ifdef OME_HAVE_XERCES_DOM
  ome::xml::OMEEntityResolver&
  get_resolver()
  {
    static ome::xml::OMEEntityResolver resolver;
    return resolver;
  }
#endif

#if OME_HAVE_QT5_DOM
  void
  parse_xml(QDomDocument& doc, QXmlInputSource& src)
  {
    int error_line, error_column;
    QString error_msg;
    bool parse_ok = doc.setContent(&src, true, &error_msg, &error_line, &error_column);

    if(!parse_ok)
      {
        std::string fs = fmt::format("XML parse error: {0}: line {1}, column {2}",
                                     error_msg.toStdString(),
                                     error_line,
                                     error_column);
        throw std::runtime_error(fs);
      }
  }
#endif

}

namespace ome
{
  namespace xml
  {

#ifdef OME_HAVE_XERCES_DOM
    DOMDocument
    createDocument(const ome::compat::filesystem::path&          file,
                   const ome::common::xml::dom::ParseParameters& params)
    {
      ome::xml::OMEEntityResolver& resolver = get_resolver();
      return ome::common::xml::dom::createDocument(file, resolver, params);
    }

    DOMDocument
    createDocument(const std::string&                            text,
                   const ome::common::xml::dom::ParseParameters& params,
                   const std::string&                            id)
    {
      ome::xml::OMEEntityResolver& resolver = get_resolver();
      return ome::common::xml::dom::createDocument(text, resolver, params, id);
    }

    DOMDocument
    createDocument(std::istream&                                 stream,
                   const ome::common::xml::dom::ParseParameters& params,
                   const std::string&                            id)
    {
      ome::xml::OMEEntityResolver& resolver = get_resolver();
      return ome::common::xml::dom::createDocument(stream, resolver, params, id);
    }
#elif OME_HAVE_QT5_DOM
    DOMDocument
    createDocument(const ome::compat::filesystem::path& file)
    {
      QDomDocument doc;
      QString qfile = QString::fromUtf8(file.string().c_str());
      QFile qf(qfile);
      QXmlInputSource src(&qf);

      parse_xml(doc, src);

      return doc;
    }

    DOMDocument
    createDocument(const std::string& text,
                   const std::string& /* id */)
    {
      QDomDocument doc;
      QByteArray raw_data(text.c_str(), text.size());
      QXmlInputSource src;
      src.setData(raw_data);

      parse_xml(doc, src);

      return doc;
    }

    DOMDocument
    createDocument(std::istream&      stream,
                   const std::string& id)
    {
      // Inefficient use of memory.
      std::string input;

      stream.seekg(0, std::ios::end);
      input.reserve(stream.tellg());
      stream.seekg(0, std::ios::beg);

      input.assign(std::istreambuf_iterator<char>(stream),
                   std::istreambuf_iterator<char>());

      return createDocument(input, id);
    }
#endif

  }
}
