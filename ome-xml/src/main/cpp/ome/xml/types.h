/*
 * #%L
 * OME-XML C++ library for working with OME-XML metadata structures.
 * %%
 * Copyright © 2019 Quantitative Imaging Systems, LLC
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of any organization.
 * #L%
 */

#ifndef OME_XML_TYPES_H
#define OME_XML_TYPES_H

#include <ome/xml/config.h>

#ifdef OME_HAVE_XERCES_DOM

#include <ome/xerces-util/dom/Element.h>
#include <ome/xerces-util/dom/Document.h>
#include <ome/xerces-util/dom/Node.h>
#include <ome/xerces-util/dom/NodeList.h>

namespace ome
{
  namespace xml
  {

    using DOMDocument = ::ome::common::xml::dom::Document;
    using DOMElement = ::ome::common::xml::dom::Element;
    using DOMNode = ::ome::common::xml::dom::Node;
    using DOMNodeList = ::ome::common::xml::dom::NodeList;
    using DOMNamedNodeMap = ::ome::common::xml::dom::NamedNodeMap;
    using XMLInputSource = ::xercesc::InputSource;

  }
}

#elif OME_HAVE_QT5_DOM

#include <QtXml/QDomDocument>
#include <QtXml/QDomElement>
#include <QtXml/QDomNode>
#include <QtXml/QDomNodeList>

namespace ome
{
  namespace xml
  {

    using DOMDocument = QDomDocument;
    using DOMElement = QDomElement;
    using DOMNode = QDomNode;
    using DOMNodeList = QDomNodeList;
    using DOMNamedNodeMap = QDomNamedNodeMap;
    using XMLInputSource = QXmlInputSource;

  }
}

#else
#error No XML DOM implementation defined
#endif

#endif // OME_XML_TYPES_H
