/*
 * #%L
 * OME-XML C++ library for working with OME-XML metadata structures.
 * %%
 * Copyright © 2015 - 2016 Open Microscopy Environment:
 *   - Massachusetts Institute of Technology
 *   - National Institutes of Health
 *   - University of Dundee
 *   - Board of Regents of the University of Wisconsin-Madison
 *   - Glencoe Software, Inc.
 * Copyright © 2019 Codelibre Consulting Limited
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of any organization.
 * #L%
 */

#include <cassert>
#include <deque>
#include <fstream>
#include <iostream>
#include <set>
#include <utility>

#include <ome/compat/filesystem.h>

#include <ome/common/log.h>
#include <ome/common/module.h>

#include <ome/xml/module.h>
#include <ome/xml/OMEEntityResolver.h>

#ifdef OME_HAVE_XERCES_DOM
#include <ome/xerces-util/String.h>
#include <ome/xerces-util/Platform.h>
#include <ome/xerces-util/dom/Document.h>
#include <ome/xerces-util/dom/Element.h>
#include <ome/xerces-util/dom/NodeList.h>

#include <xercesc/sax/InputSource.hpp>
#include <xercesc/framework/MemBufInputSource.hpp>
#elif OME_HAVE_QT5_DOM
#include <QtCore/QUrl>
#endif

#include <fmt/format.h>

using ome::common::LOG_TRACE;
using ome::common::LOG_DEBUG;

namespace ome
{
  namespace xml
  {

    OMEEntityResolver::OMEEntityResolver():
      BaseEntityResolver(),
#if OME_HAVE_QT5_XSLT
      QAbstractUriResolver(),
#endif
      entity_path_map(),
      entity_data_map()
    {
      // Hack to force module registration when static linking.
      register_module_paths();

      registerCatalog(ome::common::module_runtime_path("ome-xml-schema") / "catalog.xml");
    }

    OMEEntityResolver::~OMEEntityResolver()
    {
    }

#ifdef OME_HAVE_XERCES_DOM
    xercesc::InputSource *
    OMEEntityResolver::resolveEntity(xercesc::XMLResourceIdentifier* resource)
    {
      xercesc::InputSource *ret = 0;

      if (resource)
        {
          switch(resource->getResourceIdentifierType())
            {
            case xercesc::XMLResourceIdentifier::SchemaGrammar:
            case xercesc::XMLResourceIdentifier::SchemaImport:
              {
                ret = getSource(ome::common::xml::String(resource->getSchemaLocation()));
              }
              break;
            case xercesc::XMLResourceIdentifier::ExternalEntity:
              {
                ret = getSource(ome::common::xml::String(resource->getSystemId()));
              }
              break;
            default:
              break;
            }
        }

      return ret;
    }
#elif OME_HAVE_QT5_DOM
    bool
    OMEEntityResolver::resolveEntity(const QString& publicId,
                                     const QString& systemId,
                                     QXmlInputSource*& ret)
    {
      std::cerr << "QT resolve entity publicId=" << publicId.toStdString()
                << "  systemId=" << systemId.toStdString() << std::endl;

      /// @todo Resolve from catalogue.
      ret = nullptr;

      return true;
    }

    QString
    OMEEntityResolver::errorString() const
    {
      return QString::fromUtf8("Failed to resolve entity");
    }

#if OME_HAVE_QT5_XSLT
    QUrl
    OMEEntityResolver::resolve(const QUrl &relative,
                               const QUrl &baseURI) const
    {
      /// @todo Resolve from catalogue.

      return baseURI.resolved(relative);
    }
#endif
#endif

    XMLInputSource *
    OMEEntityResolver::getSource(const std::string& resource)
    {
      XMLInputSource *ret = 0;

      entity_path_map_type::const_iterator i = entity_path_map.find(resource);

      if (i != entity_path_map.end())
        {
          entity_data_map_type::iterator d = entity_data_map.find(resource);

          if (d == entity_data_map.end()) // No cached data
            {
              const ome::compat::filesystem::path& file(i->second);

              if (ome::compat::filesystem::exists(file))
                {
                  std::ifstream in(file.string().c_str());
                  if (in)
                    {
                      std::string data;
                      std::ios::pos_type pos = in.tellg();
                      in.seekg(0, std::ios::end);
                      std::ios::pos_type len = in.tellg() - pos;
                      if (len)
                        data.reserve(static_cast<std::string::size_type>(len));
                      in.seekg(0, std::ios::beg);

                      data.assign(std::istreambuf_iterator<char>(in),
                                  std::istreambuf_iterator<char>());

                      LOG_DEBUG("Registering resource data {} ({})", resource, i->second);

                      std::pair<entity_data_map_type::iterator,bool> valid =
                        entity_data_map.insert(std::make_pair(resource, data));
                      if (valid.second)
                        d = valid.first;
                    }
                  else
                    {
                      std::string fs = fmt::format("Failed to load XML schema id ‘{0}’ from file ‘{1}’",
                                                   resource, file.string());
                      std::cerr << fs << '\n';
                    }
                }
            }

          if (d != entity_data_map.end()) // Cached data
            {
              const std::string&data(d->second);

              LOG_TRACE("Returning resource {} ({})", resource, i->second);

#ifdef OME_HAVE_XERCES_DOM
              ret = new xercesc::MemBufInputSource(reinterpret_cast<const XMLByte *>(data.c_str()),
                                                   static_cast<XMLSize_t>(data.size()),
                                                   ome::common::xml::String(i->second.string()));
#elif OME_HAVE_QT5_DOM
              ret = new QXmlInputSource();
              QByteArray raw_data(data.c_str(), data.size());
              ret->setData(raw_data);
#endif
            }
        }

      return ret;
    }

    void
    OMEEntityResolver::registerEntity(const std::string&                   id,
                                      const ome::compat::filesystem::path& file)
    {
      entity_path_map_type::iterator i = entity_path_map.find(id);

      if (i == entity_path_map.end())
        {
          // Insert new entry.
          entity_path_map.insert(std::make_pair(id, canonical(file)));
        }
      else
        {
          if(canonical(file) != i->second)
            {
              std::string fs = fmt::format("Mismatch registering entity id ‘{0}’: File ‘{1}’ does not match existing cached file ‘{2}’",
                                           id, i->second.string(), file.string());
              std::cerr << fs << std::endl;
              throw std::runtime_error(fs);
            }
        }
    }

    void
    OMEEntityResolver::registerCatalog(const ome::compat::filesystem::path& catalog)
    {
      std::set<ome::compat::filesystem::path> visited;
      std::deque<ome::compat::filesystem::path> pending;

#ifdef OME_HAVE_XERCES_DOM
      ome::common::xml::Platform platform;
#endif

      pending.push_back(ome::compat::filesystem::canonical(catalog));

      while(!pending.empty())
        {
          ome::compat::filesystem::path current(pending.front());
          assert(!current.empty());
          pending.pop_front();

          if (visited.find(current) != visited.end())
            {
              std::string fs = fmt::format("XML catalog ‘{}’ contains a recursive reference",
                                           current.string());
              std::cerr << fs << '\n';
              continue; // This has already been processed; break loop
            }

          ome::compat::filesystem::path currentdir(current.parent_path());
          assert(!currentdir.empty());

#ifdef OME_HAVE_XERCES_DOM
          std::ifstream in(current.string().c_str());
          if (in)
            {
              EntityResolver r; // Does nothing.
              DOMDocument doc(ome::common::xml::dom::createDocument(in, r,
                                                                    ome::common::xml::dom::ParseParameters(),
                                                                    current.string()));
              DOMElement root(doc.getDocumentElement());
              DOMNodeList nodes(root.getChildNodes());

              for (auto& node : nodes)
                {
                  if (node.getNodeType() == xercesc::DOMNode::ELEMENT_NODE)
                    {
                      DOMElement e(node.get(), false);
                      if (e)
                        {
                          if (e.getTagName() == "uri")
                            {
                              if (e.hasAttribute("uri") && e.hasAttribute("name"))
                                {
                                  ome::compat::filesystem::path newid(currentdir / static_cast<std::string>(e.getAttribute("uri")));

                                  LOG_DEBUG("Registering {} as {}",
                                            static_cast<std::string>(e.getAttribute("name")),
                                            ome::compat::filesystem::canonical(newid).string());
                                  registerEntity(static_cast<std::string>(e.getAttribute("name")),
                                                 ome::compat::filesystem::canonical(newid));
                                }
                            }
                          if (e.getTagName() == "nextCatalog")
                            {
                              if (e.hasAttribute("catalog"))
                                {
                                  ome::compat::filesystem::path newcatalog(currentdir / static_cast<std::string>(e.getAttribute("catalog")));
                                  pending.push_back(ome::compat::filesystem::canonical(newcatalog));
                                }
                            }
                        }
                    }
                }
            }
#ifndef NDEBUG
          // Don't make failure hard in release builds; just skip.
          else
            {
              std::string fs = fmt::format("Failed to load XML catalog from file ‘{}’",
                                           catalog.string());
              throw std::runtime_error(fs);
            }
#endif
#elif OME_HAVE_QT5_DOM
          QFile in(current.string().c_str());
          DOMDocument doc;
          QXmlInputSource src(&in);

          int error_line, error_column;
          QString error_msg;
          bool parse_ok = doc.setContent(&src, true, &error_msg, &error_line, &error_column);

#ifndef NDEBUG
          if(!parse_ok)
            {
              std::string fs = fmt::format("XML catalog parse error: {0}: line {1}, column {2}",
                                           error_msg.toStdString(),
                                           error_line,
                                           error_column);
              throw std::runtime_error(fs);
            }
#endif

          DOMElement root(doc.documentElement());
          DOMNodeList nodes(root.childNodes());

          for (int ni = 0; ni < nodes.size(); ++ni)
            {
              DOMNode node = nodes.at(ni);
              if (node.isElement())
                {
                  DOMElement e = node.toElement();
                  if (!e.isNull())
                    {
                      if (e.tagName() == "uri")
                        {
                          if (e.hasAttribute("uri") && e.hasAttribute("name"))
                            {
                              ome::compat::filesystem::path newid(currentdir / e.attribute("uri").toStdString());

                              LOG_DEBUG("Registering {} as {}",
                                        e.attribute("name").toStdString(),
                                        ome::compat::filesystem::canonical(newid).string());
                              registerEntity(e.attribute("name").toStdString(),
                                             ome::compat::filesystem::canonical(newid));
                            }
                        }
                      if (e.tagName() == "nextCatalog")
                        {
                          if (e.hasAttribute("catalog"))
                            {
                              ome::compat::filesystem::path newcatalog(currentdir / e.attribute("catalog").toStdString());
                              pending.push_back(ome::compat::filesystem::canonical(newcatalog));
                            }
                        }
                    }
                }
            }
#endif

          // Mark this file visited.
          visited.insert(current);
        }
    }

  }
}
